// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDaofMUWqNDzoEeGtZC71uHHJHO01Qke9E",
    authDomain: "dailyreport-ffb01.firebaseapp.com",
    databaseURL: "https://dailyreport-ffb01.firebaseio.com",
    projectId: "dailyreport-ffb01",
    storageBucket: "dailyreport-ffb01.appspot.com",
    messagingSenderId: "619315127400",
    appId: "1:619315127400:web:1cbcee2aaee04df09547dd",
    measurementId: "G-J64MDS0EZG"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
