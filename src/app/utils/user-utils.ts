import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import _ from 'lodash';
import { CustomCookieService } from '../modules/shared/services/custom-cookie/custom-cookie.service';
import { USER } from '../consts/cookies.const';
import { AppState } from '../app.state';
import { Store } from '@ngrx/store';
import { getCurrentUser, saveUserInStore } from '../modules/users/users.actions';
import { User } from '../models/user.model';
import * as fromUsers from '../modules/users/users.selectors';

@Injectable()
export class UserUtils {
  constructor(
    private cookieService: CustomCookieService,
    private store: Store<AppState>
  ) {}

  /**
   * Return if user is signed in
   */
  userIsSignedIn(): Observable<boolean> {
    return new Observable((observer) => {
      let cookieUser: string = this.cookieService.getCookie(USER);
      if (!_.isNil(cookieUser) && cookieUser !== '') {
        let user = JSON.parse(cookieUser);
        if (!_.isNil(user)) {
          this.store.dispatch(
            saveUserInStore({
              user: new User().deserialize(user),
            })
          );
          observer.next(true);
        } else {
          observer.next(false);
        }
      } else {
        observer.next(false);
      }
      observer.complete();
    });
  }

  /**
   * Update user in cookies after user name change
   */
  updateUserInCookie(newUser: User): void {
    let cookieUser: string = this.cookieService.getCookie(USER);
    if (!_.isNil(cookieUser) && cookieUser !== '') {
      let user = JSON.parse(cookieUser);
      if (!_.isNil(user)) {
        user.displayName = newUser.displayName;
        user.photoUrl = newUser.photoUrl;
        this.cookieService.setCookie(USER, JSON.stringify(user));
        this.store.dispatch(
          saveUserInStore({
            user: new User().deserialize(user),
          })
        );
      }
    }
  }
}
