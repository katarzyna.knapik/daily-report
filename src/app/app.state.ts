import { UsersStoreInterface } from './modules/users/users.reducer';


export interface AppState {
    readonly users: UsersStoreInterface
}