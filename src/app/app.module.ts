import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { UsersModule } from './modules/users/users.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { SharedModule } from './modules/shared/shared.module';
import { AuthGuard } from './auth.guard';
import { StoreModule } from '@ngrx/store';
import { UsersReducer } from './modules/users/users.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { UsersEffects } from './modules/users/users.effects';
import { UserUtils } from './utils/user-utils';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientModule } from '@angular/common/http';
import { DailyDataModule } from './modules/daily-data/daily-data.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    UsersModule,
    MatSnackBarModule,
    SharedModule,
    DailyDataModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatButtonModule,
    MatSidenavModule,
    StoreModule.forRoot({
      users: UsersReducer,
    }),
    StoreDevtoolsModule.instrument(),
    EffectsModule.forRoot([UsersEffects]),
    ReactiveFormsModule,
  ],
  providers: [AuthGuard, UserUtils],
  bootstrap: [AppComponent],
})
export class AppModule {}
