export interface AuthGuardInterface {
    authNeeded: boolean;
}