import { RoleInterface } from '../consts/roles.const';
import { UserInterfaceApiResponse } from '../modules/users/service/api/sign-in.interface';
import { Deserializable } from './serialization.interface';

export class User implements Deserializable {
  public idToken: string;
  public email: string;
  public emailVerified: boolean;
  public photoUrl: string;
  public refreshToken: string;
  public expiresIn: string;
  public localId: string;
  public displayName: string;
  public registered?: boolean;

  deserialize(input: UserInterfaceApiResponse) {
    Object.assign(this, input);
    return this;
  }

}
