import { Action, createReducer, on } from '@ngrx/store';
import {
  FORM_STATUS_EMAIL_SENT,
  FORM_STATUS_INITIAL,
  FORM_STATUS_CODE_VERIFIED_SUCCESS,
  FORM_STATUS_CODE_VERIFIED_ERROR,
  FORM_STATUS_NEW_PASSWORD_SET,
  FORM_STATUS_NEW_USER_REGISTERED,
} from 'src/app/consts/form-status.const';
import { User } from 'src/app/models/user.model';
import * as UsersAction from './users.actions';

export const USERS_REDUCER: string = 'users';

export interface PasswordResetInterface {
  email: string;
  formStatus: string;
}

export interface AddUserInterface {
  email: string;
  formStatus: string;
}

export interface UsersStoreInterface {
  currentUser: User;
  passwordReset: PasswordResetInterface;
  registerUser: AddUserInterface;
}

export const initialState: UsersStoreInterface = {
  currentUser: null,
  passwordReset: {
    email: '',
    formStatus: FORM_STATUS_INITIAL,
  },
  registerUser: {
    email: '',
    formStatus: FORM_STATUS_INITIAL,
  },
};

const usersReducer = createReducer(
  initialState,
  /**
   * Log in user
   */
  on(UsersAction.loginUserWithEmailAndPassword, (state) => ({
    ...state,
    currentUser: initialState.currentUser,
  })),
  on(UsersAction.loginUserWithEmailAndPasswordSuccess, (state, { user }) => ({
    ...state,
    currentUser: user,
  })),
  on(UsersAction.saveUserInStore, (state, { user }) => ({
    ...state,
    currentUser: user,
  })),
  /**
  * Register user
  */
  on(UsersAction.registerUserWithEmailAndPassword, (state) => ({
    ...state,
    registerUser: initialState.registerUser,
  })),
  on(UsersAction.registerUserWithEmailAndPasswordClearForm, (state) => ({
    ...state,
    registerUser: initialState.passwordReset,
  })),
  on(
    UsersAction.registerUserWithEmailAndPasswordSuccess,
    (state, { userEmail }) => ({
      ...state,
      registerUser: {
        email: userEmail,
        formStatus: FORM_STATUS_NEW_USER_REGISTERED,
      },
    })
  ),
  /**
   * Send password reset email
   */
  on(UsersAction.sendPasswordResetEmail, (state) => ({
    ...state,
    passwordReset: initialState.passwordReset,
  })),
  on(UsersAction.sendPasswordResetEmailClearForm, (state) => ({
    ...state,
    passwordReset: initialState.passwordReset,
  })),
  on(UsersAction.sendPasswordResetEmailSuccess, (state, { userEmail }) => ({
    ...state,
    passwordReset: {
      email: userEmail,
      formStatus: FORM_STATUS_EMAIL_SENT,
    },
  })),
  /**
   * Verify password reset code
   */
  on(UsersAction.verifyPasswordResetCode, (state) => ({
    ...state,
    passwordReset: initialState.passwordReset,
  })),
  on(UsersAction.verifyPasswordResetCodeSuccess, (state, { userEmail }) => ({
    ...state,
    passwordReset: {
      email: userEmail,
      formStatus: FORM_STATUS_CODE_VERIFIED_SUCCESS,
    },
  })),
  on(UsersAction.verifyPasswordResetCodeError, (state) => ({
    ...state,
    passwordReset: {
      email: initialState.passwordReset.email,
      formStatus: FORM_STATUS_CODE_VERIFIED_ERROR,
    },
  })),
  /**
   * Confirm password reset
   */
  on(UsersAction.confirmPasswordReset, (state) => ({
    ...state,
    passwordReset: initialState.passwordReset,
  })),
  on(UsersAction.confirmPasswordResetSuccess, (state, { userEmail }) => ({
    ...state,
    passwordReset: {
      email: userEmail,
      formStatus: FORM_STATUS_NEW_PASSWORD_SET,
    },
  })),
  /**
   * Sign out user
   */
  on(UsersAction.signOutSuccess, (state) => ({
    ...state,
    currentUser: initialState.currentUser,
  })),
  /**
   * Sign out user
   */
  on(UsersAction.deleteAccountSuccess, (state) => ({
    ...state,
    currentUser: initialState.currentUser,
  }))

);

export function UsersReducer(
  state: UsersStoreInterface = initialState,
  action: Action
) {
  return usersReducer(state, action);
}
