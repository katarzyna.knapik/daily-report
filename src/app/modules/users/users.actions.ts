import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/models/user.model';

/**
 * Login user with email and password
 */
export const LOGIN_USER_WITH_EMAIL_AND_PASSWORD: string =
  'LOGIN_USER_WITH_EMAIL_AND_PASSWORD';
export const LOGIN_USER_WITH_EMAIL_AND_PASSWORD_SUCCESS: string =
  'LOGIN_USER_WITH_EMAIL_AND_PASSWORD_SUCCESS';
export const LOGIN_USER_WITH_EMAIL_AND_PASSWORD_ERROR: string =
  'LOGIN_USER_WITH_EMAIL_AND_PASSWORD_ERROR';
export const SAVE_USER_IN_STORE: string = 'SAVE_USER_IN_STORE';
export const loginUserWithEmailAndPassword = createAction(
  LOGIN_USER_WITH_EMAIL_AND_PASSWORD,
  props<{ userEmail: string; password: string }>()
);
export const loginUserWithEmailAndPasswordSuccess = createAction(
  LOGIN_USER_WITH_EMAIL_AND_PASSWORD_SUCCESS,
  props<{ user: User }>()
);
export const loginUserWithEmailAndPasswordError = createAction(
  LOGIN_USER_WITH_EMAIL_AND_PASSWORD_ERROR
);
export const saveUserInStore = createAction(
  SAVE_USER_IN_STORE,
  props<{ user: User }>()
);

/**
 * Get user
 */
export const GET_CURRENT_USER: string = 'GET_CURRENT_USER';
export const GET_CURRENT_USER_SUCCESS: string =
  'GET_CURRENT_USER_SUCCESS';
export const GET_CURRENT_USER_ERROR: string =
  'GET_CURRENT_USER_ERROR';
export const getCurrentUser = createAction(GET_CURRENT_USER);
export const getCurrentUserSuccess = createAction(
  GET_CURRENT_USER_SUCCESS,
  props<{ user: User }>()
);
export const getCurrentUserError = createAction(
  GET_CURRENT_USER_ERROR
);

/**
 * Register user with email and password
 */
export const REGISTER_USER_WITH_EMAIL_AND_PASSWORD: string =
  'REGISTER_USER_WITH_EMAIL_AND_PASSWORD';
export const REGISTER_USER_WITH_EMAIL_AND_PASSWORD_CLEAR_FORM: string =
  'REGISTER_USER_WITH_EMAIL_AND_PASSWORD_CLEAR_FORM';
export const ADD_NEW_USER_TO_FIREBASE: string = 'ADD_NEW_USER_TO_FIREBASE';
export const SAVE_USERS_DISPLAY_NAME: string = 'SAVE_USERS_DISPLAY_NAME';
export const REGISTER_USER_WITH_EMAIL_AND_PASSWORD_SUCCESS: string =
  'REGISTER_USER_WITH_EMAIL_AND_PASSWORD_SUCCESS';
export const REGISTER_USER_WITH_EMAIL_AND_PASSWORD_ERROR: string =
  'REGISTER_USER_WITH_EMAIL_AND_PASSWORD_ERROR';
export const registerUserWithEmailAndPassword = createAction(
  REGISTER_USER_WITH_EMAIL_AND_PASSWORD,
  props<{
    user: User;
    password: string;
  }>()
);
export const registerUserWithEmailAndPasswordClearForm = createAction(
  REGISTER_USER_WITH_EMAIL_AND_PASSWORD_CLEAR_FORM
);
export const addNewUserToFirebase = createAction(
  ADD_NEW_USER_TO_FIREBASE,
  props<{
    user: User;
  }>()
);
export const saveUsersDisplayName = createAction(
  SAVE_USERS_DISPLAY_NAME,
  props<{
    user: User;
  }>()
);
export const registerUserWithEmailAndPasswordSuccess = createAction(
  REGISTER_USER_WITH_EMAIL_AND_PASSWORD_SUCCESS,
  props<{ userEmail: string }>()
);
export const registerUserWithEmailAndPasswordError = createAction(
  REGISTER_USER_WITH_EMAIL_AND_PASSWORD_ERROR
);

/**
 * Send password reset email
 */
export const SEND_PASSWORD_RESET_EMAIL: string = 'SEND_PASSWORD_RESET_EMAIL';
export const SEND_PASSWORD_RESET_EMAIL_SUCCESS: string =
  'SEND_PASSWORD_RESET_EMAIL_SUCCESS';
export const SEND_PASSWORD_RESET_EMAIL_ERROR: string =
  'SEND_PASSWORD_RESET_EMAIL_ERROR';
export const SEND_PASSWORD_RESET_EMAIL_CLEAR_FORM: string =
  'SEND_PASSWORD_RESET_EMAIL_CLEAR_FORM';
export const sendPasswordResetEmail = createAction(
  SEND_PASSWORD_RESET_EMAIL,
  props<{ userEmail: string }>()
);
export const sendPasswordResetEmailSuccess = createAction(
  SEND_PASSWORD_RESET_EMAIL_SUCCESS,
  props<{ userEmail: string }>()
);
export const sendPasswordResetEmailError = createAction(
  SEND_PASSWORD_RESET_EMAIL_ERROR
);
export const sendPasswordResetEmailClearForm = createAction(
  SEND_PASSWORD_RESET_EMAIL_CLEAR_FORM
);

/**
 * Verify password reset code
 */
export const VERIFY_PASSWORD_RESET_CODE: string = 'VERIFY_PASSWORD_RESET_CODE';
export const VERIFY_PASSWORD_RESET_CODE_SUCCESS: string =
  'VERIFY_PASSWORD_RESET_CODE_SUCCESS';
export const VERIFY_PASSWORD_RESET_CODE_ERROR: string =
  'VERIFY_PASSWORD_RESET_CODE_ERROR';
export const verifyPasswordResetCode = createAction(
  VERIFY_PASSWORD_RESET_CODE,
  props<{ code: string }>()
);
export const verifyPasswordResetCodeSuccess = createAction(
  VERIFY_PASSWORD_RESET_CODE_SUCCESS,
  props<{ userEmail: string }>()
);
export const verifyPasswordResetCodeError = createAction(
  VERIFY_PASSWORD_RESET_CODE_ERROR
);

/**
 * Confirm password reset
 */
export const CONFIRM_PASSWORD_RESET: string = 'CONFIRM_PASSWORD_RESET';
export const CONFIRM_PASSWORD_RESET_SUCCESS: string =
  'CONFIRM_PASSWORD_RESET_SUCCESS';
export const CONFIRM_PASSWORD_RESET_ERROR: string =
  'CONFIRM_PASSWORD_RESET_ERROR';
export const confirmPasswordReset = createAction(
  CONFIRM_PASSWORD_RESET,
  props<{ code: string; password: string }>()
);
export const confirmPasswordResetSuccess = createAction(
  CONFIRM_PASSWORD_RESET_SUCCESS,
  props<{ userEmail: string }>()
);
export const confirmPasswordResetError = createAction(
  CONFIRM_PASSWORD_RESET_ERROR
);

/**
 * Sign out
 */
export const SIGN_OUT: string = 'SIGN_OUT';
export const SIGN_OUT_SUCCESS: string = 'SIGN_OUT_SUCCESS';
export const SIGN_OUT_ERROR: string = 'SIGN_OUT_ERROR';
export const signOut = createAction(SIGN_OUT);
export const signOutSuccess = createAction(SIGN_OUT_SUCCESS);
export const signOutError = createAction(SIGN_OUT_ERROR);

/**
 * Update user
 */
export const UPDATE_USER: string = 'UPDATE_USER';
export const UPDATE_USER_SUCCESS: string = 'UPDATE_USER_SUCCESS';
export const UPDATE_USER_ERROR: string = 'UPDATE_USER_ERROR';
export const updateUser = createAction(
  UPDATE_USER,
  props<{
    user: User;
  }>()
);
export const updateUserSuccess = createAction(
  UPDATE_USER_SUCCESS,
  props<{
    user: User;
  }>()
);
export const updateUserError = createAction(UPDATE_USER_ERROR);

/**
 * Delete account
 */
export const DELETE_ACCOUNT: string = 'DELETE_ACCOUNT';
export const REMOVE_USER_FROM_FIREBASE: string = 'REMOVE_USER_FROM_FIREBASE';
export const DELETE_ACCOUNT_SUCCESS: string = 'DELETE_ACCOUNT_SUCCESS';
export const DELETE_ACCOUNT_ERROR: string = 'DELETE_ACCOUNT_ERROR';
export const deleteAccount = createAction(DELETE_ACCOUNT, props<{
  user: User;
}>());
export const removeUserFromFirebase = createAction(REMOVE_USER_FROM_FIREBASE, props<{
  user: User;
}>());
export const deleteAccountSuccess = createAction(DELETE_ACCOUNT_SUCCESS);
export const deleteAccountError = createAction(DELETE_ACCOUNT_ERROR);