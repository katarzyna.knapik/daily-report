import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.state';
import { PASSWORD_RECOVERY, REGISTER } from 'src/app/consts/router.const';
import { loginUserWithEmailAndPassword } from '../../users.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordFormControl = new FormControl('', [Validators.required]);
  loginForm = new FormGroup({
    emailFormControl: this.emailFormControl,
    passwordFormControl: this.passwordFormControl,
  });

  constructor(private store: Store<AppState>, private router: Router) {}

  /**
   * Go to register page
   */
  register(): void {
    this.router.navigate([REGISTER]);
  }

  /**
   * Go to password recovery
   */
  forgotPassword(): void {
    this.router.navigate([PASSWORD_RECOVERY]);
  }

  /**
   * Login user
   */
  loginUser(): void {
    this.store.dispatch(
      loginUserWithEmailAndPassword({
        userEmail: this.loginForm.value.emailFormControl,
        password: this.loginForm.value.passwordFormControl,
      })
    );
  }

  /**
   * On init
   */
  ngOnInit(): void {}
}
