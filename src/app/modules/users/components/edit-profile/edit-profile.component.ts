import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { select, Store } from '@ngrx/store';
import { AppState } from 'src/app/app.state';
import { User } from 'src/app/models/user.model';
import { ConfirmDialogComponent, ConfirmDialogComponentInput } from 'src/app/modules/shared/components/confirm-dialog/confirm-dialog.component';
import { deleteAccount, updateUser } from '../../users.actions';
import * as fromUsers from '../../users.selectors';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss'],
})
export class EditProfileComponent implements OnInit {
  public user: User;
  public userImage: string;
  displayNameFormControl = new FormControl('', [Validators.required]);
  emailFormControl = new FormControl({ value: '', disabled: true });
  editProfileForm = new FormGroup({
    displayNameFormControl: this.displayNameFormControl,
    emailFormControl: this.emailFormControl
  });

  constructor(
    private dialogRef: MatDialogRef<EditProfileComponent>,
    private dialog: MatDialog,
    private store: Store<AppState>
  ) { }

  /**
   * Get user image or default
   */
  getImage(): string {
    return this.userImage;
  }

  /**
   * Save user profile
   */
  saveProfile(): void {
    let newUser: User = new User();
    Object.assign(newUser, this.user)
    newUser.displayName = this.editProfileForm.value.displayNameFormControl;
    this.store.dispatch(
      updateUser({
        user: newUser
      })
    );
  }

  /**
   * Cancel save
   */
  close(): void {
    this.dialogRef.close();
  }

  handleFileInput(event: Event) {
    // const reader = new FileReader();
    // reader.addEventListener('load', (event) => {

    //   console.log(event.target.result)

    //   this.userImage = <string>event.target.result;
    // });
    // reader.readAsDataURL(event.target.files[0]);

  }

  /**
   * Removing account
   */
  removeAccount(): void {
    let data: ConfirmDialogComponentInput = {
      title: 'Confirmation',
      info: 'You will loose all data. Are you sure?',
      okButton: 'Remove all data',
      cancelButton: 'Cancel'
    }
    this.close();
    this.dialog.open(ConfirmDialogComponent, {
      data: data
    })
      .afterClosed()
      .subscribe((confirmed) => {
        if (confirmed) {
          this.store.dispatch(
            deleteAccount({
              user: this.user
            })
          );
        }
      });
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.store
      .pipe(select(fromUsers.selectCurrentUser))
      .subscribe((currentUser) => {
        this.user = currentUser;
        this.userImage = currentUser.photoUrl;
        this.displayNameFormControl.setValue(this.user.displayName);
        this.emailFormControl.setValue(this.user.email);
      });
  }
}
