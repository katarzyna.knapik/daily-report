import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppState } from 'src/app/app.state';
import {
  FORM_STATUS_INITIAL,
  FORM_STATUS_NEW_USER_REGISTERED,
} from 'src/app/consts/form-status.const';
import { LOGIN } from 'src/app/consts/router.const';
import { User } from 'src/app/models/user.model';
import {
  registerUserWithEmailAndPassword,
  registerUserWithEmailAndPasswordClearForm,
} from '../../users.actions';
import * as fromUsers from '../../users.selectors';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../login/login.component.scss'],
})
export class RegisterComponent implements OnInit {
  userEmail: string;
  formState: string;

  firstNameFormControl = new FormControl('', [Validators.required]);
  lastNameFormControl = new FormControl('', [Validators.required]);
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordFormControl = new FormControl('', [Validators.required]);
  passwordConfirmFormControl = new FormControl('', [Validators.required]);
  registerForm = new FormGroup(
    {
      firstNameFormControl: this.firstNameFormControl,
      lastNameFormControl: this.lastNameFormControl,
      emailFormControl: this.emailFormControl,
      passwordFormControl: this.passwordFormControl,
      passwordConfirmFormControl: this.passwordConfirmFormControl,
    },
    {
      validators: this.checkPasswordsValidator,
    }
  );

  constructor(private store: Store<AppState>, private router: Router) {}

  /**
   * Matching passwords validators
   */
  checkPasswordsValidator(group: FormGroup): ValidationErrors | null {
    let password = group.value.passwordFormControl;
    let confirmPassword = group.value.passwordConfirmFormControl;
    return password === confirmPassword ? null : { notMatch: true };
  }

  /**
   * Go to login page
   */
  login(): void {
    this.router.navigate([LOGIN]);
  }

  /**
   * Check if new user created
   */
  newUserCreated(): boolean {
    return this.formState === FORM_STATUS_NEW_USER_REGISTERED;
  }

  /**
   * Check if initial state
   */
  initialFormState(): boolean {
    return this.formState === FORM_STATUS_INITIAL;
  }

  /**
   * Register new user
   */
  signUp(): void {
    let user: User = new User();
    user.email = this.registerForm.value.emailFormControl;
    user.displayName =
      this.registerForm.value.firstNameFormControl +
      ' ' +
      this.registerForm.value.lastNameFormControl;
    this.store.dispatch(
      registerUserWithEmailAndPassword({
        user: user,
        password: this.registerForm.value.passwordFormControl
      })
    );
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.store.dispatch(registerUserWithEmailAndPasswordClearForm());
    this.store.pipe(select(fromUsers.selectNewUserEmail)).subscribe((email) => {
      this.userEmail = email;
    });
    this.store
      .pipe(select(fromUsers.selectNewUserFormStatus))
      .subscribe((formStatus) => {
        this.formState = formStatus;
      });
  }
}
