import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppState } from 'src/app/app.state';
import {
  FORM_STATUS_CODE_VERIFIED_ERROR,
  FORM_STATUS_CODE_VERIFIED_SUCCESS,
  FORM_STATUS_NEW_PASSWORD_SET,
} from 'src/app/consts/form-status.const';
import { LOGIN, PASSWORD_RECOVERY } from 'src/app/consts/router.const';
import {
  confirmPasswordReset,
  verifyPasswordResetCode,
} from '../../users.actions';
import * as fromUsers from '../../users.selectors';
import * as _ from 'lodash';

@Component({
  selector: 'app-password-recovery-change',
  templateUrl: './password-recovery-change.component.html',
  styleUrls: ['../login/login.component.scss'],
})
export class PasswordRecoveryChangeComponent implements OnInit {
  userEmail: string;
  formState: string;
  oobCode: string;

  passwordFormControl = new FormControl('', [Validators.required]);
  passwordConfirmFormControl = new FormControl('', [Validators.required]);
  passwordChangeRecoveryForm = new FormGroup(
    {
      passwordFormControl: this.passwordFormControl,
      passwordConfirmFormControl: this.passwordConfirmFormControl,
    },
    {
      validators: this.checkPasswordsValidator,
    }
  );

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  /**
   * Matching passwords validators
   */
  checkPasswordsValidator(group: FormGroup): ValidationErrors | null {
    let password = group.value.passwordFormControl;
    let confirmPassword = group.value.passwordConfirmFormControl;
    return password === confirmPassword ? null : { notMatch: true };
  }

  /**
   * Go to register page
   */
  login(): void {
    this.router.navigate([LOGIN]);
  }

  /**
   * Check if there is form error
   */
  codeExpired(): boolean {
    return (
      this.formState === FORM_STATUS_CODE_VERIFIED_ERROR ||
      _.isNil(this.oobCode)
    );
  }

  /**
   * Check if there is form error
   */
  codeIsOK(): boolean {
    return this.formState === FORM_STATUS_CODE_VERIFIED_SUCCESS;
  }

  /**
   * New password was set
   */
  newPasswordSet(): boolean {
    return this.formState === FORM_STATUS_NEW_PASSWORD_SET;
  }

  /**
   * Go to password recovery
   */
  forgotPassword(): void {
    this.router.navigate([PASSWORD_RECOVERY]);
  }

  /**
   * Check if passwords match
   */
  passwordsMatch(): boolean {
    return (
      this.passwordChangeRecoveryForm.value.passwordFormControl ===
      this.passwordChangeRecoveryForm.value.passwordConfirmFormControl
    );
  }

  /**
   * Set new password
   */
  changePasswordRecovery(): void {
    this.store.dispatch(
      confirmPasswordReset({
        code: this.oobCode,
        password: this.passwordChangeRecoveryForm.value.passwordFormControl,
      })
    );
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.oobCode = params['oobCode'];
      if (!_.isNil(this.oobCode)) {
        this.store.dispatch(
          verifyPasswordResetCode({
            code: this.oobCode,
          })
        );
      }
    });
    this.store
      .pipe(select(fromUsers.selectPasswordResetEmail))
      .subscribe((email) => {
        this.userEmail = email;
      });
    this.store
      .pipe(select(fromUsers.selectPasswordResetFormStatus))
      .subscribe((formStatus) => {
        this.formState = formStatus;
      });
  }
}
