import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppState } from 'src/app/app.state';
import {
  FORM_STATUS_EMAIL_SENT,
  FORM_STATUS_INITIAL,
} from 'src/app/consts/form-status.const';
import { LOGIN } from 'src/app/consts/router.const';
import {
  sendPasswordResetEmail,
  sendPasswordResetEmailClearForm,
} from '../../users.actions';
import * as fromUsers from '../../users.selectors';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['../login/login.component.scss'],
})
export class PasswordRecoveryComponent implements OnInit {
  userEmail: string;
  formState: string;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordRecoveryForm = new FormGroup({
    emailFormControl: this.emailFormControl,
  });

  constructor(private store: Store<AppState>, private router: Router) {}

  /**
   * Go to register page
   */
  login(): void {
    this.router.navigate([LOGIN]);
  }

  /**
   * Check if initial state
   */
  initialFormState(): boolean {
    return this.formState === FORM_STATUS_INITIAL;
  }

  /**
   * Check if email was sent
   */
  emailSent(): boolean {
    return this.formState === FORM_STATUS_EMAIL_SENT;
  }

  /**
   * Send password recovery email
   */
  sendPasswordRecovery(): void {
    this.store.dispatch(
      sendPasswordResetEmail({
        userEmail: this.passwordRecoveryForm.value.emailFormControl,
      })
    );
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.store.dispatch(sendPasswordResetEmailClearForm());
    this.store
      .pipe(select(fromUsers.selectPasswordResetEmail))
      .subscribe((email) => {
        this.userEmail = email;
      });
    this.store
      .pipe(select(fromUsers.selectPasswordResetFormStatus))
      .subscribe((formStatus) => {
        this.formState = formStatus;
      });
  }
}
