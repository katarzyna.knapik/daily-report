import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError, tap, withLatestFrom, switchMap } from 'rxjs/operators';
import _, { merge } from 'lodash';
import * as actions from './users.actions';
import { DASHBOARD, LOGIN } from 'src/app/consts/router.const';
import { USER } from 'src/app/consts/cookies.const';
import { Router } from '@angular/router';
import { UsersService } from './service/users.service';
import { HttpErrorResponse } from '@angular/common/http';
import {
  UserInterfaceApiResponseError,
} from './service/api/sign-in.interface';
import { CustomCookieService } from '../shared/services/custom-cookie/custom-cookie.service';
import { SnackBarService } from '../shared/services/snack-bar/snack-bar.service';
import {
  EMAIL_EXISTS,
  EMAIL_NOT_FOUND,
  INVALID_PASSWORD,
  USER_DISABLED,
  OPERATION_NOT_ALLOWED,
  TOO_MANY_ATTEMPTS_TRY_LATER,
  EXPIRED_OOB_CODE,
  INVALID_OOB_CODE,
  WEAK_PASSWORD,
  CREDENTIAL_TOO_OLD_LOGIN_AGAIN, ACCOUNT_DELETED
} from 'src/app/consts/errors.const';
import { User } from 'src/app/models/user.model';
import { UserUtils } from 'src/app/utils/user-utils';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.state';
import { selectCurrentUser, selectNewUserEmail } from './users.selectors';

@Injectable()
export class UsersEffects {
  loginUserWithEmailAndPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.loginUserWithEmailAndPassword),
      mergeMap((action) =>
        this.userService
          .signInWithEmailAndPassword(action.userEmail, action.password)
          .pipe(
            map((user) => {
              this.cookieService.setCookie(USER, JSON.stringify(user));
              return {
                type: actions.LOGIN_USER_WITH_EMAIL_AND_PASSWORD_SUCCESS,
                user: new User().deserialize(user),
              };
            }),
            catchError((response: HttpErrorResponse) => {
              this.showError(
                (<UserInterfaceApiResponseError>response.error).error.message
              );
              return of({
                type: actions.LOGIN_USER_WITH_EMAIL_AND_PASSWORD_ERROR,
              });
            })
          )
      )
    )
  );

  loginUserWithEmailAndPasswordSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.loginUserWithEmailAndPasswordSuccess),
        tap(() => {
          this.router.navigate([DASHBOARD]).catch((error) => {
            console.log(error);
          });
        })
      ),
    {
      dispatch: false,
    }
  );

  registerUserWithEmailAndPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.registerUserWithEmailAndPassword),
      mergeMap((action) =>
        this.userService
          .signUpWithEmailAndPassword(action.user, action.password)
          .pipe(
            map((user) => {
              let newUser: User = new User().deserialize(user);
              newUser.displayName = action.user.displayName;
              return {
                type: actions.ADD_NEW_USER_TO_FIREBASE,
                user: newUser,
              };
            }),
            catchError((response: HttpErrorResponse) => {
              this.showError(
                (<UserInterfaceApiResponseError>response.error).error.message
              );
              return of({
                type: actions.REGISTER_USER_WITH_EMAIL_AND_PASSWORD_ERROR,
              });
            })
          )
      )
    )
  );

  saveUserInStore$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveUserInStore),
      map(() => {
        return {
          type: actions.GET_CURRENT_USER,
        };
      })
    )
  );

  getCurrentUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.getCurrentUser),
      withLatestFrom(this.store.select(selectCurrentUser)),
      mergeMap((currentUser) =>
        this.userService.getCurrentUser(currentUser[1].idToken)
          .pipe(
            map((user) => {
              return {
                type: actions.GET_CURRENT_USER_SUCCESS,
                user: new User().deserialize(user),
              };
            }),
            catchError((response: HttpErrorResponse) => {
              this.showError(
                (<UserInterfaceApiResponseError>response.error).error.message
              );
              return of({
                type: actions.GET_CURRENT_USER_ERROR,
              });
            })
          )
      )
    )
  )

  getCurrentUserError$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.getCurrentUserError),
        tap(() => {
          this.cookieService.removeCookie(USER);
          this.router.navigate([LOGIN]).catch((error) => {
            console.log(error);
          });
        })
      ),
    {
      dispatch: false,
    }
  );

  addNewUserToFirebase$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.addNewUserToFirebase),
      mergeMap((action) =>
        this.userService.addUserToFirestore(action.user.localId).pipe(
          map((response) => {
            if (response) {
              return {
                type: actions.SAVE_USERS_DISPLAY_NAME,
                user: action.user,
              };
            }
            return {
              type: actions.REGISTER_USER_WITH_EMAIL_AND_PASSWORD_ERROR,
            };
          }),
          catchError((response: HttpErrorResponse) => {
            this.showError(
              (<UserInterfaceApiResponseError>response.error).error.message
            );
            return of({
              type: actions.REGISTER_USER_WITH_EMAIL_AND_PASSWORD_ERROR,
            });
          })
        )
      )
    )
  );

  saveUsersDisplayName$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveUsersDisplayName),
      mergeMap((action) =>
        this.userService.updateUser(action.user).pipe(
          map(() => ({
            type: actions.REGISTER_USER_WITH_EMAIL_AND_PASSWORD_SUCCESS,
            userEmail: action.user.email,
          })),
          catchError((response: HttpErrorResponse) => {
            this.showError(
              (<UserInterfaceApiResponseError>response.error).error.message
            );
            return of({
              type: actions.REGISTER_USER_WITH_EMAIL_AND_PASSWORD_ERROR,
            });
          })
        )
      )
    )
  );

  updateUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.updateUser),
      mergeMap((action) =>
        this.userService.updateUser(action.user).pipe(
          map((user) => {
            this.userUtils.updateUserInCookie(new User().deserialize(user))
            return {
              type: actions.UPDATE_USER_SUCCESS,
              user: new User().deserialize(user),
            }
          }),
          catchError((response: HttpErrorResponse) => {
            this.showError(
              (<UserInterfaceApiResponseError>response.error).error.message
            );
            return of({
              type: actions.UPDATE_USER_ERROR,
            });
          })
        )
      )
    )
  );

  sendPasswordResetEmail$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.sendPasswordResetEmail),
      mergeMap((action) =>
        this.userService.sendPasswordResetEmail(action.userEmail).pipe(
          map((response) => ({
            type: actions.SEND_PASSWORD_RESET_EMAIL_SUCCESS,
            userEmail: response.email,
          })),
          catchError((response: HttpErrorResponse) => {
            this.showError(
              (<UserInterfaceApiResponseError>response.error).error.message
            );
            return of({
              type: actions.SEND_PASSWORD_RESET_EMAIL_ERROR,
            });
          })
        )
      )
    )
  );

  verifyPasswordResetCode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.verifyPasswordResetCode),
      mergeMap((action) =>
        this.userService.verifyPasswordResetCode(action.code).pipe(
          map((response) => ({
            type: actions.VERIFY_PASSWORD_RESET_CODE_SUCCESS,
            userEmail: response.email,
          })),
          catchError((response: HttpErrorResponse) => {
            this.showError(
              (<UserInterfaceApiResponseError>response.error).error.message
            );
            return of({
              type: actions.VERIFY_PASSWORD_RESET_CODE_ERROR,
            });
          })
        )
      )
    )
  );

  confirmPasswordReset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.confirmPasswordReset),
      mergeMap((action) =>
        this.userService
          .confirmPasswordReset(action.code, action.password)
          .pipe(
            map((response) => ({
              type: actions.CONFIRM_PASSWORD_RESET_SUCCESS,
              userEmail: response.email,
            })),
            catchError((response: HttpErrorResponse) => {
              this.showError(
                (<UserInterfaceApiResponseError>response.error).error.message
              );
              return of({
                type: actions.CONFIRM_PASSWORD_RESET_ERROR,
              });
            })
          )
      )
    )
  );

  signOut$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.signOut),
      mergeMap(() => {
        return this.userService.signOut().pipe(
          map(() => {
            return {
              type: actions.SIGN_OUT_SUCCESS,
            };
          })
        );
      })
    )
  );

  signOutSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.signOutSuccess),
        tap(() => {
          this.cookieService.removeCookie(USER);
          this.router.navigate([LOGIN]).catch((error) => {
            console.log(error);
          });
        })
      ),
    {
      dispatch: false,
    }
  );

  deleteAccount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.deleteAccount),
      mergeMap((action) => {
        return this.userService.deleteAccount(action.user).pipe(
          map(() => {
            return {
              type: actions.REMOVE_USER_FROM_FIREBASE,
              user: action.user
            };
          }),
          catchError((response: HttpErrorResponse) => {
            this.showError(
              (<UserInterfaceApiResponseError>response.error).error.message
            );
            return of({
              type: actions.DELETE_ACCOUNT_ERROR,
            });
          })
        );
      })
    )
  );

  removeUserFromFirebase$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.removeUserFromFirebase),
      mergeMap((action) => {
        return this.userService.removeUserFromFirebase(action.user.localId).pipe(
          map((response) => {
            if (response) {
              return {
                type: actions.DELETE_ACCOUNT_SUCCESS,
                user: action.user
              };
            }
            return {
              type: actions.DELETE_ACCOUNT_ERROR,
            };
          }),
          catchError((response: HttpErrorResponse) => {
            this.showError(
              (<UserInterfaceApiResponseError>response.error).error.message
            );
            return of({
              type: actions.DELETE_ACCOUNT_ERROR,
            });
          })
        );
      })
    )
  );

  deleteAccountSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.deleteAccountSuccess),
        tap(() => {
          this.cookieService.removeCookie(USER);
          this.router.navigate([LOGIN]).catch((error) => {
            console.log(error);
          });
          this.showError('ACCOUNT_DELETED');
        })
      ),
    {
      dispatch: false,
    }
  );

  /**
   * Show API error
   * @param errorMessage
   */
  showError(errorMessage: string) {
    switch (errorMessage) {
      case 'EMAIL_NOT_FOUND':
        this.snackBar.showSimpleSnackBar(EMAIL_NOT_FOUND);
        break;
      case 'INVALID_PASSWORD':
        this.snackBar.showSimpleSnackBar(INVALID_PASSWORD);
        break;
      case 'USER_DISABLED':
        this.snackBar.showSimpleSnackBar(USER_DISABLED);
        break;
      case 'EMAIL_EXISTS':
        this.snackBar.showSimpleSnackBar(EMAIL_EXISTS);
        break;
      case 'OPERATION_NOT_ALLOWED':
        this.snackBar.showSimpleSnackBar(OPERATION_NOT_ALLOWED);
        break;
      case 'TOO_MANY_ATTEMPTS_TRY_LATER':
        this.snackBar.showSimpleSnackBar(TOO_MANY_ATTEMPTS_TRY_LATER);
        break;
      case 'OPERATION_NOT_ALLOWED':
        this.snackBar.showSimpleSnackBar(OPERATION_NOT_ALLOWED);
        break;
      case 'EXPIRED_OOB_CODE':
        this.snackBar.showSimpleSnackBar(EXPIRED_OOB_CODE);
        break;
      case 'INVALID_OOB_CODE':
        this.snackBar.showSimpleSnackBar(INVALID_OOB_CODE);
        break;
      case 'WEAK_PASSWORD : Password should be at least 6 characters':
        this.snackBar.showSimpleSnackBar(WEAK_PASSWORD);
      case 'ACCOUNT_DELETED':
        this.snackBar.showSimpleSnackBar(ACCOUNT_DELETED);
        break;
      case 'CREDENTIAL_TOO_OLD_LOGIN_AGAIN':
      case 'INVALID_ID_TOKEN':
        this.snackBar.showSimpleSnackBar(CREDENTIAL_TOO_OLD_LOGIN_AGAIN);
    }
  }

  constructor(
    private actions$: Actions,
    private userService: UsersService,
    private router: Router,
    private cookieService: CustomCookieService,
    private snackBar: SnackBarService,
    private userUtils: UserUtils,
    private store: Store<AppState>
  ) { }
}
