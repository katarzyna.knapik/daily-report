import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/auth.guard';
import { ClientsComponent } from './components/clients/clients.component';
import {
  CLIENTS,
  LOGIN,
  PASSWORD_RECOVERY,
  REGISTER,
  PASSWORD_RECOVERY_CHANGE,
} from 'src/app/consts/router.const';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { RegisterComponent } from './components/register/register.component';
import { UsersService } from './service/users.service';
import { PasswordRecoveryComponent } from './components/password-recovery/password-recovery.component';
import { StoreModule } from '@ngrx/store';
import { UsersReducer, USERS_REDUCER } from './users.reducer';
import { PasswordRecoveryChangeComponent } from './components/password-recovery-change/password-recovery-change.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    ClientsComponent,
    LoginComponent,
    RegisterComponent,
    PasswordRecoveryComponent,
    PasswordRecoveryChangeComponent,
    EditProfileComponent,
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatInputModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: LOGIN,
        component: LoginComponent,
        canActivate: [AuthGuard],
        data: {
          authNeeded: false,
        },
      },
      {
        path: REGISTER,
        component: RegisterComponent,
        canActivate: [AuthGuard],
        data: {
          authNeeded: false,
        },
      },
      {
        path: PASSWORD_RECOVERY,
        component: PasswordRecoveryComponent,
        canActivate: [AuthGuard],
        data: {
          authNeeded: false,
        },
      },
      {
        path: PASSWORD_RECOVERY_CHANGE,
        component: PasswordRecoveryChangeComponent,
        canActivate: [AuthGuard],
        data: {
          authNeeded: false,
        },
      },
      {
        path: CLIENTS,
        component: ClientsComponent,
        canActivate: [AuthGuard],
        data: {
          authNeeded: true,
        },
      },
    ]),
    StoreModule.forFeature(USERS_REDUCER, UsersReducer),
  ],
  providers: [UsersService],
  entryComponents: [EditProfileComponent],
  exports: [EditProfileComponent],
})
export class UsersModule {}
