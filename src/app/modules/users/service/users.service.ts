import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { defer } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UserInterfaceApiResponse } from './api/sign-in.interface';
import { EmailInterfaceApiResponse } from './api/email.interface';
import { CLIENT } from 'src/app/consts/roles.const';
import { User } from 'src/app/models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  // private users: Observable<UserApiInterface[]>;

  constructor(
    private firestore: AngularFirestore,
    private httpClient: HttpClient
  ) {
    // this.users = this.firestore
    //   .collection('users')
    //   .valueChanges() as Observable<UserApiInterface[]>;
  }

  /**
   * Sign in with email and password
   * @param email
   * @param password
   */
  signInWithEmailAndPassword(
    email: string,
    password: string
  ): Observable<UserInterfaceApiResponse> {
    return this.httpClient.post(
      'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=' +
      environment.firebaseConfig.apiKey,
      {
        email: email,
        password: password,
        returnSecureToken: true,
      }
    ) as Observable<UserInterfaceApiResponse>;
  }

  /**
   * Send password reset email
   * @param email
   */
  sendPasswordResetEmail(email: string): Observable<EmailInterfaceApiResponse> {
    return this.httpClient.post(
      'https://identitytoolkit.googleapis.com/v1/accounts:sendOobCode?key=' +
      environment.firebaseConfig.apiKey,
      {
        email: email,
        requestType: 'PASSWORD_RESET',
      }
    ) as Observable<EmailInterfaceApiResponse>;
  }

  /**
   * Get current user
   * @param idToken
   */
  getCurrentUser(idToken: string): Observable<UserInterfaceApiResponse> {
    return this.httpClient.post(
      'https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=' +
      environment.firebaseConfig.apiKey,
      {
        idToken: idToken
      }
    ) as Observable<UserInterfaceApiResponse>;
  }

  /**
   * Verify OOB Code
   * @param code
   */
  verifyPasswordResetCode(code: string): Observable<EmailInterfaceApiResponse> {
    return this.httpClient.post(
      'https://identitytoolkit.googleapis.com/v1/accounts:resetPassword?key=' +
      environment.firebaseConfig.apiKey,
      {
        oobCode: code,
      }
    ) as Observable<EmailInterfaceApiResponse>;
  }

  /**
   * Confirm email reset
   * @param code
   * @param password
   */
  confirmPasswordReset(
    code: string,
    password: string
  ): Observable<EmailInterfaceApiResponse> {
    return this.httpClient.post(
      'https://identitytoolkit.googleapis.com/v1/accounts:resetPassword?key=' +
      environment.firebaseConfig.apiKey,
      {
        oobCode: code,
        newPassword: password,
      }
    ) as Observable<EmailInterfaceApiResponse>;
  }

  /**
   * Sign up with email and password
   * @param user
   * @param password
   */
  signUpWithEmailAndPassword(
    user: User,
    password: string
  ): Observable<UserInterfaceApiResponse> {
    return this.httpClient.post(
      'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=' +
      environment.firebaseConfig.apiKey,
      {
        email: user.email,
        password: password,
        returnSecureToken: true,
      }
    ) as Observable<UserInterfaceApiResponse>;
  }

  /**
   * Add new user to firestore
   * @param uid
   */
  addUserToFirestore(uid: string): Observable<boolean> {
    return new Observable((observer) => {
      this.firestore
        .collection('users')
        .doc(uid)
        .set({
          birthDate: null,
          height: null,
          role: CLIENT.roleId,
          weight: null,
        }).then(
          () => {
            this.firestore
              .collection('data')
              .doc(uid)
              .set({
                report: []
              })
              .then(() => {
                observer.next(true);
                observer.complete();
              })
              .catch(() => {
                observer.next(false);
                observer.complete();
              })
          }
        )
        .catch(() => {
          observer.next(false);
          observer.complete();
        })
    });
  }

  /**
   * Update user
   * @param user 
   */
  updateUser(user: User) {
    return this.httpClient.post(
      'https://identitytoolkit.googleapis.com/v1/accounts:update?key=' +
      environment.firebaseConfig.apiKey,
      {
        idToken: user.idToken,
        displayName: user.displayName,
        photoUrl: user.photoUrl,
        returnSecureToken: true,
      }
    ) as Observable<UserInterfaceApiResponse>;
  }

  /**
   * Sign out current session user
   */
  signOut(): Observable<void> {
    return defer(() => firebase.auth().signOut()) as Observable<void>;
  }


  /**
     * Delete account
     * @param user 
     */
  deleteAccount(user: User) {
    return this.httpClient.post(
      'https://identitytoolkit.googleapis.com/v1/accounts:delete?key=' +
      environment.firebaseConfig.apiKey,
      {
        idToken: user.idToken
      }
    ) as Observable<any>;
  }

  /**
   * Remove user from firestore
   * @param uid
   * @param firstName
   * @param lastName
   */
  removeUserFromFirebase(uid: string): Observable<boolean> {
    return new Observable((observer) => {
      this.firestore.collection("users").doc(uid).delete().then(
        () => {
          this.firestore.collection("data").doc(uid).delete()
            .then(() => {
              observer.next(true);
              observer.complete();
            })
            .catch(() => {
              observer.next(false);
              observer.complete();
            })
        }
      )
        .catch(() => {
          observer.next(false);
          observer.complete();
        })
    });
  }

}
