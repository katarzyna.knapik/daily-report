import { User } from 'src/app/models/user.model';

export interface UserInterfaceApiResponse {
  idToken: string;
  email: string;
  emailVerified: boolean;
  photoUrl: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  displayName: string;
  registered?: boolean;
}

export interface UserInterfaceApiResponseError {
  error: {
    code: string;
    message: string;
  };
}
