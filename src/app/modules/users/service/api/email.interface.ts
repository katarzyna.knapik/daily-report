import { HttpErrorResponse } from '@angular/common/http';

export interface EmailInterfaceApiResponse {
  email: string;
  requestType?: string;
}

export interface EmailInterfaceApiResponseError {
  error: {
    code: string;
    message: string;
  };
}

