import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UsersStoreInterface, USERS_REDUCER } from './users.reducer';

export const getUsersState = createFeatureSelector<UsersStoreInterface>(USERS_REDUCER);

/**
 * Select current user
 */
export const selectCurrentUser = createSelector(
  getUsersState,
  (state: UsersStoreInterface) => state.currentUser
);

/**
 * Select email for password reset
 */
export const selectPasswordResetEmail = createSelector(
  getUsersState,
  (state: UsersStoreInterface) => state.passwordReset.email
);

/**
 * Select form status for password reset
 */
export const selectPasswordResetFormStatus = createSelector(
  getUsersState,
  (state: UsersStoreInterface) => state.passwordReset.formStatus
);

/**
 * Select email for new user
 */
export const selectNewUserEmail = createSelector(
  getUsersState,
  (state: UsersStoreInterface) => state.registerUser.email
);

/**
 * Select form status for new user
 */
export const selectNewUserFormStatus = createSelector(
  getUsersState,
  (state: UsersStoreInterface) => state.registerUser.formStatus
);