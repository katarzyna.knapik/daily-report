import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SNACKBAR_DURATION } from 'src/app/consts/common.const';

@Injectable({
  providedIn: 'root',
})
export class SnackBarService {
  constructor(private snackBar: MatSnackBar) {}

  /**
   * Shows message
   * @param message
   */
  showSimpleSnackBar(message: string): void {
    this.snackBar.open(message, null, {
      duration: SNACKBAR_DURATION,
    });
  }
}
