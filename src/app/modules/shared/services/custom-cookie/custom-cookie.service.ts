import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root',
})
export class CustomCookieService {
  constructor(private cookieService: CookieService) {}

  /**
   * Set cookie
   * @param name
   * @param value
   */
  setCookie(name, value): void {
    this.cookieService.set(name, value);
  }

  /**
   * Get cookie
   * @param name
   */
  getCookie(name): string {
    return this.cookieService.get(name);
  }

  /**
   * Remove cookie
   * @param name
   */
  removeCookie(name): void {
    this.cookieService.delete(name);
  }
}
