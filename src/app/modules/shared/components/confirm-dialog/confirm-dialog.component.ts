import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface ConfirmDialogComponentInput {
  title: string;
  info: string;
  okButton: string;
  cancelButton: string;
}

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogComponentInput,
    private dialogRef: MatDialogRef<ConfirmDialogComponent>,

  ) { }

  /**
     * Cancel save
     */
  close(confirmed: boolean): void {
    this.dialogRef.close(confirmed);
  }

  /**
   * On init
   */
  ngOnInit(): void {
  }

}
