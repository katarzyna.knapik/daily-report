import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as fromUsers from 'src/app/modules/users/users.selectors';
import { AppState } from 'src/app/app.state';
import _ from 'lodash';
import { signOut } from 'src/app/modules/users/users.actions';
import { MatDialog } from '@angular/material/dialog';
import { EditProfileComponent } from 'src/app/modules/users/components/edit-profile/edit-profile.component';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
})
export class TopBarComponent implements OnInit {
  user: User;

  constructor(private dialog: MatDialog, private store: Store<AppState>) {}

  /**
   * Sign out current user
   */
  signOutUser(): void {
    this.store.dispatch(signOut());
  }

  /**
   * Get user image or default
   */
  getImage(): string {
    return this.user.photoUrl;
  }

  /**
   * Go to edit profile
   */
  editProfile(): void {
    this.dialog.open(EditProfileComponent);
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.store
      .pipe(select(fromUsers.selectCurrentUser))
      .subscribe((currentUser) => {
        this.user = currentUser;
      });
  }
}
