import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DataListComponent } from './components/data-list/data-list.component';
import { EntryEditComponent } from './components/entry-edit/entry-edit.component';
import { AuthGuard } from 'src/app/auth.guard';
import { RouterModule } from '@angular/router';
import { DASHBOARD } from 'src/app/consts/router.const';

@NgModule({
  declarations: [DashboardComponent, DataListComponent, EntryEditComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: DASHBOARD,
        component: DashboardComponent,
        canActivate: [AuthGuard],
        data: {
          authNeeded: true,
        },
      },
    ]),
  ],
  entryComponents: [
    EntryEditComponent
  ]
})
export class DailyDataModule {}
