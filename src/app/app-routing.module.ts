import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LOGIN } from './consts/router.const';

const routes: Routes = [
  { path: '', redirectTo: LOGIN, pathMatch: 'full' },
  { path: '*', redirectTo: LOGIN },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
