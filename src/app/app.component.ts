import { Component, OnInit } from '@angular/core';
import {
  DASHBOARD,
  LOGIN,
  PASSWORD_RECOVERY,
  PASSWORD_RECOVERY_CHANGE,
  REGISTER,
} from './consts/router.const';
import { NavigationStart, Router } from '@angular/router';
import * as fromUsers from './modules/users/users.selectors';
import { select, Store } from '@ngrx/store';
import { AppState } from './app.state';
import * as _ from 'lodash';
import { getCurrentUser } from './modules/users/users.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'DailyReport';
  public containerClass: string;
  public userIsSignedIn: boolean;

  constructor(private router: Router, private store: Store<AppState>) {}

  /**
   * Check if redirection is needed
   */
  checkRedirection(): void {
    if (this.userIsSignedIn) {
      this.router.navigate([DASHBOARD]);
    }
  }

  /**
   * Subscribe behavour based on route
   */
  subscribeBehaviour(): void {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.containerClass = '';
        if (
          event.url.substring(1) === LOGIN ||
          event.url.substring(1) === PASSWORD_RECOVERY ||
          event.url.substring(1) === REGISTER ||
          event.url.indexOf(PASSWORD_RECOVERY_CHANGE) > -1
        ) {
          this.containerClass = 'loginPage';
          this.checkRedirection();
        }
      }
    });
  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.store.pipe(select(fromUsers.selectCurrentUser)).subscribe((user) => {
      this.userIsSignedIn = !_.isNil(user);
    });
    this.subscribeBehaviour();
  }
}
