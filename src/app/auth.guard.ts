import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthGuardInterface } from './models/auth.guard.interface';
import * as _ from 'lodash';
import { UserUtils } from './utils/user-utils';
import { LOGIN } from './consts/router.const';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private userUtils: UserUtils) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return new Observable((observer) => {
      if (!(<AuthGuardInterface>route.data).authNeeded) {
        observer.next(true);
        observer.complete();
      } else {
        this.userUtils.userIsSignedIn().subscribe((userIsSigned) => {
          if (userIsSigned) {
            observer.next(true);
          } else {
            observer.next(false);
            this.router.navigate([LOGIN]);
          }
          observer.complete();
        });
      }
    });
  }
}
