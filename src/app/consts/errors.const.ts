export const EMAIL_NOT_FOUND: string =
  'User not found. Try again or contact administrator.';
export const INVALID_PASSWORD: string =
  'Wrong password. Try again or try to restore your password.';
export const USER_DISABLED: string = 'User is disabled. Contact administrator';
export const EMAIL_EXISTS: string =
  'User with this email exists. Try to restore your password or contact with administrator.';
export const OPERATION_NOT_ALLOWED: string =
  'This operation is not allowed. Contact administrator.';
export const TOO_MANY_ATTEMPTS_TRY_LATER: string =
  'Too many server ping attempt. Try again later or contact with administrator.';
export const EXPIRED_OOB_CODE: string =
  'Link has expired. Try to reset your password again or contact administrator.';
export const INVALID_OOB_CODE: string =
  'Invalid OOB code. Try to reset your password again or contact administrator.';
export const WEAK_PASSWORD: string =
  'Your password is too weak. It should be at least 6 characters.';
export const CREDENTIAL_TOO_OLD_LOGIN_AGAIN: string = 'Session expired.';
export const ACCOUNT_DELETED: string = 'Your account has been deleted';
