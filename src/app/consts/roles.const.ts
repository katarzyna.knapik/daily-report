export interface RoleInterface {
  roleId: number;
  roleDescription: string;
}

export const CLIENT: RoleInterface = {
  roleId: 1,
  roleDescription: 'Client role',
};

export const TRAINER: RoleInterface = {
  roleId: 2,
  roleDescription: 'Trainer role',
};

export const ROLES: RoleInterface[] = [CLIENT, TRAINER];

export function asUserRole(roleId: number): RoleInterface {
  let userRole: RoleInterface;
  ROLES.forEach((role) => {
    if (role.roleId === roleId) {
      userRole = role;
    }
  });
  return userRole;
}
