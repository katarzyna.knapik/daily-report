export const CLIENTS: string = 'clients';
export const LOGIN: string = 'login';
export const REGISTER: string = 'register';
export const PASSWORD_RECOVERY: string = 'password-recovery';
export const PASSWORD_RECOVERY_CHANGE: string = 'password-change';
export const DASHBOARD: string = 'dashboard';